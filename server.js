"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var cacheServer = require("./cacheServer");
var port = process.env.port || 6379;
var bunyan = require('bunyan');
var RedisServer = require('redis-server');
var config = {
    defaultDomain: 'http://127.0.0.1:3000/',
    port: 3030,
    instanceName: 'CACHE',
    redisConfig: {
        host: '127.0.0.1',
        port: 6379,
    },
    cacheRules: {
        default: 'always',
        always: [],
        never: [],
        maxAge: []
    }
};
var logger = bunyan.createLogger({ name: 'Lis Rejestrator' });
var server = new cacheServer.CacheServer(config, logger);
server.start(function (err) {
    if (err)
        throw err;
    console.log('Serwer odpalony');
});
var serwer = new RedisServer(port);
serwer.open(function (err) {
    if (err === null) {
        // You may now connect a client to the Redis
        // server bound to port 6379.
    }
});
//# sourceMappingURL=server.js.map