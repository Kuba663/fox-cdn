﻿import cacheServer = require('./cacheServer');
import http = require("http");
var path = require("path");
var port = process.env.port || 6379
var bunyan = require('bunyan');
let RedisServer = require('redis-server');
let AsyncTask = require("async-task");
var BackgroundWorker = require('background-worker');
import express = require("express");

let app = express();
let HttpServer = http.createServer(app);

var config = {
    defaultDomain: 'http://127.0.0.1:3000/',
    port: 3030,
    instanceName: 'CACHE',
    redisConfig: {
        host: '127.0.0.1',
        port: port,
    },
    cacheRules: {
        default: 'always',
        always: [],
        never: [],
        maxAge: []
    }
}

var logger = bunyan.createLogger({ name: 'Lis Rejestrator' });
var server = new cacheServer.CacheServer(config, logger);

var worker = new BackgroundWorker({})

server.start(function (err) {
    if (err) throw err;
    console.log('Serwer odpalony');
});


app.set('port', 5000);
app.use('/www', express.static(__dirname + '/www'));

app.get('/www', function (request, response) {
    response.sendFile(path.join(__dirname, 'index.html'));
});