"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redis_url_cache_1 = require("redis-url-cache");
var nodeurl = require("url");
var request = require('request');
var iconv = require('iconv-lite');
var debug = require('debug')('fox-cdn');
var RedisCache = /** @class */ (function () {
    function RedisCache() {
    }
    RedisCache.connectRedisUrlCache = function (instanceName, redisConfig, defaultDomain, cb) {
        var instance = new redis_url_cache_1.Instance(instanceName, redisConfig, {}, function (err) {
            if (err)
                return cb(err);
            RedisCache.cacheEngine = new redis_url_cache_1.CacheEngineCB(defaultDomain, instance);
            cb(null);
        });
    };
    return RedisCache;
}());
exports.RedisCache = RedisCache;
var CacheServerRequest = /** @class */ (function () {
    //todo prepend: ")]}',\n" to all JSON responses
    function CacheServerRequest(defaultDomain, logger, request) {
        this.defaultDomain = defaultDomain;
        this.url = nodeurl.parse(request.url, true);
        this.logger = logger.child({
            script: 'CacheServer',
            url: this.url
        });
        this.validateURL(request);
        try {
            this.urlCB = RedisCache.cacheEngine.url(this.url.query.url);
        }
        catch (e) {
            this.logger.error({ e: e }, 'Error executin cacheEngine.url()');
            debug('Error with url: request.url', request.url, e);
            throw e;
        }
        //todo
        //Replicate all the headers from the original request
    }
    CacheServerRequest.prototype.validateURL = function (request) {
        if (this.url.pathname !== '/get' || typeof this.url.query.url === 'undefined') {
            var error = new Error("Error - the URL is not valid - only '{hostname}/get?url=' is allowed");
            this.logger.error({ err: error });
            debug(error.message, this.url.pathname, this.url.query);
            throw error;
        }
        this.headers = request.headers;
        if (typeof this.headers['referer'] === 'undefined') {
            if (typeof this.url.query.referer !== 'undefined') {
                // This is only for debug purposes
                this.headers['referer'] = this.url.query.referer;
            }
            else {
                var error = new Error('Error - the referer header is not set');
                debug('no referer: headers = ', request.headers);
                this.logger.error({ headers: request.headers, err: error });
                throw error;
            }
        }
        var parsedOriginalURL = nodeurl.parse(request.headers['referer'].toString());
        parsedOriginalURL.query = null;
        parsedOriginalURL.path = null;
        parsedOriginalURL.pathname = null;
        parsedOriginalURL.hash = null;
        parsedOriginalURL.search = null;
        this.originalURL = nodeurl.format(parsedOriginalURL);
        //debug('original headers = ', this.headers);
        //delete this.headers['ngreferer'];
    };
    CacheServerRequest.prototype.getIt = function (cb) {
        var _this = this;
        debug('getIt called');
        this.detectCachingStatus(function (cachingStatus) {
            //debug('detectCachingStatus', cachingStatus);
            switch (cachingStatus) {
                case 'ERROR':
                case 'NEVER':
                case 'NOT_CACHED':
                    debug('IT IS NOT CACHED');
                    _this.requestURL(false, _this.headers, function (err, result) {
                        debug('INSIDE GETIT CB');
                        if (err) {
                            debug('ERROR while requesting ', _this.url.query.url, err);
                            _this.logger.error({ err: err, cachingStatus: cachingStatus, headers: _this.headers }, err);
                            return cb(501, { 'Content-Type': 'text/html' }, 'Error ' + err);
                        }
                        if (result.content.length === 0) {
                            debug('ERROR while requesting ', _this.url.query.url);
                            _this.logger.error({ cachingStatus: cachingStatus, headers: _this.headers }, 'Empty response');
                            return cb(501, { 'Content-Type': 'text/html' }, 'Error response is empty' + JSON.stringify(result));
                        }
                        result.headers['caching-status'] = cachingStatus;
                        if (cachingStatus !== 'NEVER') {
                            _this.urlCB.set(result.content, {
                                status: result.status,
                                url: _this.urlCB.getUrl(),
                                domain: _this.urlCB.getDomain(),
                                headers: result.headers
                            }, false, function (err, setResult) {
                                if (err) {
                                    //todo log the error
                                    debug(err);
                                    _this.logger.error({ err: err, cachingStatus: cachingStatus, headers: _this.headers }, 'Error storing in redis-url-cache');
                                    return cb(501, { 'Content-Type': 'text/html' }, 'Error ' + err);
                                }
                                result.headers['ngServerCached'] = 'yes';
                                return cb(result.status, result.headers, result.content);
                            });
                        }
                        else {
                            result.headers['ngServerCached'] = 'no';
                            return cb(result.status, result.headers, result.content);
                        }
                    });
                    break;
                case 'CACHED':
                    debug('IT IS CACHED');
                    _this.urlCB.get(function (err, content) {
                        if (err) {
                            //todo log the error
                            debug(err);
                            _this.logger.error({ err: err, cachingStatus: cachingStatus, headers: _this.headers }, "error retrieve content from redis-url-cache");
                            return cb(501, { 'Content-Type': 'text/html' }, 'Error ' + err);
                        }
                        content.extra.headers['caching-status'] = cachingStatus;
                        content.extra.headers['ngServerCached'] = 'yes';
                        return cb(content.extra.status, content.extra.headers, content.content);
                    });
            }
        });
    };
    //convert to utf-8
    CacheServerRequest.prototype.decode = function (headers, body) {
        //debug('DECODE CALLED', headers, body.substr(0, 30));
        var re = /charset=([^()<>@,;:\"/[\]?.=\s]*)/i;
        if (headers['content-type']) {
            var charset = re.test(headers['content-type']) ? re.exec(headers['content-type'])[1] : 'utf-8';
            //debug('charset detected: ', charset);
            if (charset === 'utf-8') {
                return body;
            }
            var ic = new iconv.Iconv(charset, 'UTF-8');
            var buffer = ic.convert(body);
            return buffer.toString('utf-8');
        }
        throw new Error('content-type is missing');
    };
    CacheServerRequest.prototype.requestURL = function (binary, headers, cb) {
        var _this = this;
        //debug('CALLING REQUEST URL with headers!', headers);
        var newHeaders = {};
        newHeaders['origin'] = this.originalURL;
        newHeaders['user-agent'] = headers['user-agent'] ? headers['user-agent'] : 'Super User Agent';
        if (typeof newHeaders['accept-encoding'] !== 'undefined') {
            delete newHeaders['accept-encoding'];
            //todo enable GZIP compression - but then find a way to detect if the server supports gzip/deflate before sending the request - and set encoding = null
            //newHeaders['accept-encoding'] = headers['accept-encoding'] ? headers['accept-encoding'] : 'gzip, deflate';
            // to enable Gzip compression, use the following code:
            /**
             *
             *
             req.on('response', (res: http.IncomingMessage) => {
            let output;
             if( res.headers['content-encoding'] === 'gzip' ) {
                const gzip = zlib.createGunzip();
                res.pipe(gzip);
                output = gzip;
            } else if(res.headers['content-encoding'] === 'deflate' ) {
                const deflate = zlib.createDeflate();
                res.pipe(deflate);
                output = deflate;
            }
             else {
                output = res;
            }
             output.on('end', function() {
                debug('on END');
                callback(null, output.toString('UTF-8'));
            });
            const callback = (err: Error, body: string) => {
            debug('callback called');
            if(err) {
                return cb(err, dataResponse);
            }
            dataResponse.content = body;
            dataResponse.headers['content-length'] = body.length + '';
            dataResponse.headers['content-encoding'] = 'identity';
            debug('RESPONSE: ', dataResponse.headers, dataResponse.status, dataResponse.content.substr(0, 30));
            cb(null, dataResponse);
        }
             */
        }
        if (headers['cookie'])
            newHeaders['cookie'] = headers['cookie'];
        //debug('requestURL, sending Headers to ', this.url.query.url, JSON.stringify(newHeaders));
        var parsedURL = nodeurl.parse(this.url.query.url);
        var url = parsedURL.host === null ? this.defaultDomain + this.url.query.url : this.url.query.url;
        debug('GOING TO REQUEST', url, newHeaders);
        request({
            url: url,
            headers: newHeaders
        }, function (err, response, body) {
            //debug('INSIDE CALLBACK');
            if (err) {
                debug('Error caught in request callback', err);
                return cb(err, null);
            }
            //debug('body received, body.length  = ', body.length);
            /*try {
             body = this.decode(response.headers, body);
             } catch(e) {
             return cb(e, null);
             }
             debug('after decoding, body.length = ', body.length);
             */
            var dataResponse = {
                status: response.statusCode,
                content: body,
                headers: _this.extractHeaders(response.headers)
            };
            //debug('RESPONSE HEADERS', dataResponse.headers);
            //debug('body length = ', body.length);
            cb(null, dataResponse);
        });
    };
    CacheServerRequest.prototype.extractHeaders = function (receivedHeaders) {
        var headers = {};
        var headersToExtract = [
            'access-control-allow-origin',
            'cache-control',
            'content-encoding',
            'content-type',
            'etag',
            'set-cookie',
            'vary',
            'connection',
            'expires',
            'date',
            'last-modified'
        ];
        var keys = Object.keys(receivedHeaders);
        var newReceivedHeaders = {};
        keys.forEach(function (key) {
            newReceivedHeaders[key.toLowerCase()] = receivedHeaders[key];
        });
        headersToExtract.forEach(function (name) {
            if (newReceivedHeaders[name]) {
                headers[name] = newReceivedHeaders[name];
            }
        });
        headers['access-control-allow-origin'] = '*';
        return headers;
    };
    CacheServerRequest.prototype.detectCachingStatus = function (cb) {
        var _this = this;
        this.urlCB.has(function (err, isCached) {
            if (err) {
                _this.logger.error({ err: err }, 'Error has()');
                return cb('ERROR');
            }
            if (_this.urlCB.getCategory() === 'never') {
                return cb('NEVER');
            }
            if (isCached) {
                return cb('CACHED');
            }
            else {
                return cb('NOT_CACHED');
            }
        });
    };
    return CacheServerRequest;
}());
exports.CacheServerRequest = CacheServerRequest;
//# sourceMappingURL=cacheServerRequest.js.map