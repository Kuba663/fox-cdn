"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var cacheServerRequest_1 = require("./cacheServerRequest");
var nodeurl = require("url");
var http = require("http");
var request = require('request');
var debug = require('debug')('fox-cdn');
var CacheServer = /** @class */ (function () {
    function CacheServer(cdnConfig, logger) {
        var _this = this;
        this.cdnConfig = cdnConfig;
        this.logger = logger;
        this.urlServer = function (request, response) {
            response.setHeader('Access-Control-Allow-Origin', '*');
            response.setHeader('Access-Control-Request-Method', '*');
            response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
            debug('cdn-server URL, METHOD = ', request.method, request.url);
            if (typeof request.headers['access-control-request-headers'] !== 'undefined') {
                response.setHeader('Access-Control-Allow-Headers', request.headers['access-control-request-headers']);
            }
            if (request.method === 'OPTION' || request.method === 'OPTIONS') {
                debug('OPTION call for ', request.url);
                debug(request.headers);
                //debug(request);
                //todo forward the option to destination
                response.writeHead(200);
                response.end();
                return;
            }
            else {
                debug('requesting ', request.method, request.url);
                _this.logger.info({ url: nodeurl.parse(request.url), method: request.method }, 'New request');
            }
            try {
                var cacheServerRequest = new cacheServerRequest_1.CacheServerRequest(_this.cdnConfig.defaultDomain, _this.logger, request);
                cacheServerRequest.getIt(function (status, headers, content) {
                    var headerKeys = Object.keys(headers);
                    response.setHeader('Access-Control-Expose-Headers', headerKeys.join(','));
                    response.writeHead(status, headers);
                    //debug('ACTUALLY GOING TO SEND BACK headers = ', headers);
                    response.end(content);
                });
            }
            catch (e) {
                debug("Exception caught", e);
                response.writeHead(501, {});
                response.end(e.message);
            }
        };
    }
    CacheServer.prototype.connectRedisUrlCache = function (cb) {
        cacheServerRequest_1.RedisCache.connectRedisUrlCache(this.cdnConfig.instanceName, this.cdnConfig.redisConfig, this.cdnConfig.defaultDomain, function (err) {
            cb(err);
        });
    };
    CacheServer.prototype.start = function (cb) {
        var _this = this;
        this.httpServer = http.createServer(this.urlServer);
        this.connectRedisUrlCache(function (err) {
            if (err) {
                _this.logger.error({ err: err }, 'Error connecting with redis-url-cache');
                throw new Error(err);
            }
            _this.httpServer.listen(_this.cdnConfig.port, function (err) {
                cb(err);
            });
            _this.httpServer.on('clientError', function (err, socket) {
                debug('On Client Error: ', err);
                _this.logger.error({ err: err, socket: socket }, 'Socket error');
                socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
            });
            _this.logger.warn('Cache Server launched');
            debug('CacheServer ', _this.cdnConfig.instanceName, 'Launched');
        });
    };
    CacheServer.prototype.stop = function (cb) {
        this.httpServer.close(function (err) {
            cb(err);
        });
    };
    return CacheServer;
}());
exports.CacheServer = CacheServer;
//# sourceMappingURL=cacheServer.js.map